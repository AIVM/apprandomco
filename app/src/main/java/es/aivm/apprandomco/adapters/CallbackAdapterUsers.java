package es.aivm.apprandomco.adapters;

import java.util.ArrayList;

import es.aivm.apprandomco.model.User;

/**
 * Created by AIVM on 26/05/2018.
 */
public interface CallbackAdapterUsers {

    public void refreshList(ArrayList<User> users, boolean aloneData);
    public void refreshListFavorites(ArrayList<User> users);//solo tablet
}
