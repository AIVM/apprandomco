package es.aivm.apprandomco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;

import es.aivm.apprandomco.R;
import es.aivm.apprandomco.activities.UserDetailActivity;
import es.aivm.apprandomco.model.User;
import es.aivm.apprandomco.util.Util;

public class AdapterListUsers extends RecyclerView.Adapter<AdapterListUsers.UsersViewHolder>{

    private static final String TAG = "RandomCo";

    private ArrayList<User> users;
    private Activity activity;

    public AdapterListUsers(ArrayList<User> users, Activity activity) {
        this.users = users;
        this.activity = activity;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new UsersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        User user = users.get(position);
        holder.bindUser(user, position);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public void deleteUser(final User user){
        //construimos dialogo para consultar si esta seguro de borrar el usuario, si es asi llamamos al Callback
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Aviso")
                .setMessage("¿Esta seguro de querer borrar el usuario?")
                .setPositiveButton("Si, borrar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                users.remove(user);
                                ((CallbackAdapterUsers)activity).refreshList(users, false);//false = actualizar tanto la vista como los datos
                                Toast.makeText(activity.getApplicationContext(),
                                        "El usuario se borro correctamente", Toast.LENGTH_SHORT).show();
                            }
                        })
                .setNegativeButton("No, no borrar",null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateUser(User user, int index){
        users.set(index,user);
        if (Util.isTablet(activity.getApplicationContext())){
            ((CallbackAdapterUsers) activity).refreshListFavorites(users);
        }else {
            ((CallbackAdapterUsers) activity).refreshList(users, true);//true = actualizar solo los datos
        }
    }

    public class UsersViewHolder extends RecyclerView.ViewHolder{

        private TextView twUserName, twEmail, twNumPhone;
        private ImageView imgAvatar;
        private ImageButton ibtnFavorite, ibtnDelete;

        public UsersViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.avatar_user);
            twUserName = itemView.findViewById(R.id.full_name_user);
            twEmail = itemView.findViewById(R.id.email_user);
            twNumPhone = itemView.findViewById(R.id.cell_user);
            ibtnFavorite = itemView.findViewById(R.id.favourite_user);
            ibtnDelete = itemView.findViewById(R.id.delete_user);
        }

        public void bindUser(final User user, final int position){
            String fullname = user.getName().get("first")+" "+user.getName().get("last");
            twUserName.setText(fullname);
            twEmail.setText(user.getEmail());
            twNumPhone.setText(user.getNumPhone());

            //descargamos y mostramos listado de imagenes correspondientes en cada fila
            Glide.with(imgAvatar.getContext())
                    .load(user.getPicture().get("thumbnail"))
                    .fitCenter()
                    .into(imgAvatar);

            if (user.isFavorite()) {
                ibtnFavorite.setBackground(activity.getResources().getDrawable(R.drawable.ic_favourite_filled));
            }else{
                ibtnFavorite.setBackground(activity.getResources().getDrawable(R.drawable.ic_favourite));
            }

            //configuro listeners
            ibtnFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.isFavorite()) {
                        ibtnFavorite.setBackground(activity.getResources().getDrawable(R.drawable.ic_favourite));
                        user.setFavorite(false);
                    }else{
                        ibtnFavorite.setBackground(activity.getResources().getDrawable(R.drawable.ic_favourite_filled));
                        user.setFavorite(true);
                    }
                    updateUser(user, position);
                }
            });


            View.OnClickListener listenerImgAvatar,  listenerBtnDelete;
            if (!Util.isTablet(activity.getApplicationContext())){//Opciones solo disponibles en versión movil

                listenerImgAvatar = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Context context = view.getContext();

                        String jsonUser = new Gson().toJson(user);

                        Intent intent = new Intent(context, UserDetailActivity.class);
                        intent.putExtra(UserDetailActivity.EXTRA_NAME, jsonUser);

                        context.startActivity(intent);
                    }
                };

                listenerBtnDelete = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user.isFavorite()){
                            Snackbar.make(view,"No puede borrar un usuario favorito.",Snackbar.LENGTH_LONG).show();
                        }else{
                            deleteUser(user);
                        }
                    }
                };

            }else{
                listenerImgAvatar = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(view,"Contenido no disponible.", Snackbar.LENGTH_LONG).show();
                    }
                };

                listenerBtnDelete = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(view,"Contenido no disponible.", Snackbar.LENGTH_LONG).show();
                    }
                };
            }
            imgAvatar.setOnClickListener(listenerImgAvatar);
            ibtnDelete.setOnClickListener(listenerBtnDelete);
        }
    }
}
