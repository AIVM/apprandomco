package es.aivm.apprandomco.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class User implements Comparable<User>{

    @SerializedName("gender")
    private String gender;

    @SerializedName("name")
    private HashMap<String,String> name;

    @SerializedName("email")
    private String email;

    @SerializedName("picture")
    private HashMap<String, String> picture;

    @SerializedName("cell")
    private String numPhone;

    @SerializedName("location")
    private HashMap<String,String> location;

    @SerializedName("favorite")
    private boolean favorite;

    @SerializedName("registered")
    private String dateRegistered;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public HashMap<String, String> getPicture() {
        return picture;
    }

    public void setPicture(HashMap<String, String> picture) {
        this.picture = picture;
    }

    public String getNumPhone() {
        return numPhone;
    }

    public void setNumPhone(String numPhone) {
        this.numPhone = numPhone;
    }

    public HashMap<String, String> getLocation() {
        return location;
    }

    public void setLocation(HashMap<String, String> location) {
        this.location = location;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(String dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    @Override
    public int compareTo(@NonNull User userO) {
        return name.get("first").compareTo(userO.getName().get("first"));
    }
}
