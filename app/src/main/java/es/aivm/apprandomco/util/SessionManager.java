package es.aivm.apprandomco.util;

import android.content.Context;
import android.content.SharedPreferences;

import es.aivm.apprandomco.R;

/**
 * Created by AIVM on 27/05/2018.
 */
public class SessionManager {

    private static SharedPreferences cache;

    public static void initCache(Context context){
        SessionManager.cache = context.getSharedPreferences(String.valueOf(R.string.app_name),0);
    }

    public static void resetCache(){
        cache.edit().clear().apply();
    }

    public static String getUsersCache(){
        return cache.getString("users","");
    }
    public static void setUsersCache(String jsonUsers){
        cache.edit().putString("users", jsonUsers).apply();
    }
    public static boolean containsUsersCache(){
        return cache.contains("users");
    }

}
