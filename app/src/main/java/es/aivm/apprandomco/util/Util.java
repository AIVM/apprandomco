package es.aivm.apprandomco.util;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Created by AIVM on 26/05/2018.
 */
public class Util {

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
