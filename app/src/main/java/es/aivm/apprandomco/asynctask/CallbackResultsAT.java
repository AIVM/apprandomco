package es.aivm.apprandomco.asynctask;

public interface CallbackResultsAT {
	public void getResults(String result);
	public void getErrors(int error);
}
