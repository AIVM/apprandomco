package es.aivm.apprandomco.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.Objects;

import es.aivm.apprandomco.R;
import es.aivm.apprandomco.model.User;

public class UserDetailActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final String jsonUser = intent.getStringExtra(EXTRA_NAME);

        User user = new Gson().fromJson(jsonUser,User.class);

        ImageView imgAvatarDetail = findViewById(R.id.avatar_user_detail);
        TextView twUserNameDetail = findViewById(R.id.full_name_user_detail);
        TextView twUserEmailDetail = findViewById(R.id.email_user_detail);
        TextView twUserCellDetail = findViewById(R.id.cell_user_detail);
        TextView twUserGenderDetail = findViewById(R.id.gender_user_detail);
        TextView twUserDateDetail = findViewById(R.id.date_user_detail);
        TextView twUserLocationDetail = findViewById(R.id.location_user_detail);

        if (user != null && !user.equals("")){
            Glide.with(getApplicationContext())
                    .load(user.getPicture().get("medium"))
                    .fitCenter()
                    .into(imgAvatarDetail);

            String fullName = user.getName().get("first")+" "+user.getName().get("last");
            String fullLocation = user.getLocation().get("street")+", "+user.getLocation().get("city")
                                +" "+user.getLocation().get("state");
            twUserNameDetail.setText(fullName);
            twUserEmailDetail.setText(user.getEmail());
            twUserCellDetail.setText(user.getNumPhone());
            twUserGenderDetail.setText(user.getGender());
            twUserDateDetail.setText(user.getDateRegistered());
            twUserLocationDetail.setText(fullLocation);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
