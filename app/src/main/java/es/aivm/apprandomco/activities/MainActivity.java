package es.aivm.apprandomco.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import es.aivm.apprandomco.R;
import es.aivm.apprandomco.adapters.AdapterListUsers;
import es.aivm.apprandomco.adapters.CallbackAdapterUsers;
import es.aivm.apprandomco.asynctask.CallbackResultsAT;
import es.aivm.apprandomco.asynctask.WebServicesAT;
import es.aivm.apprandomco.model.User;
import es.aivm.apprandomco.util.SessionManager;
import es.aivm.apprandomco.util.Util;

public class MainActivity extends AppCompatActivity implements CallbackResultsAT, CallbackAdapterUsers {

    private static final String TAG = "RandomCo";

    private static final String URL = "https://api.randomuser.me/";
    private static final int LIMIT_USER = 200;
    private static final int NUM_ADD_USER = 20;

    private RecyclerView recyclerView;
    private Button btnAdd;

    private boolean aloneFavorite;

    private RecyclerView recyclerViewFavorite;
    boolean isTablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);//lista unica version movil
        recyclerView.setHasFixedSize(true);

        SessionManager.initCache(getApplicationContext());

        isTablet = Util.isTablet(getApplicationContext());

        boolean containsData;

        if (SessionManager.containsUsersCache()){
            //recuperamos datos de la cache
            ArrayList<User> usersList = getUsersListCache();
            setAdapter(usersList, recyclerView);
            containsData = true;

        }else {//solicitamos datos al servidor
            containsData = false;
            requestData(NUM_ADD_USER + 20, recyclerView);
        }

        if (isTablet){
            recyclerViewFavorite = findViewById(R.id.recyclerview_favorite);
            recyclerViewFavorite.setHasFixedSize(true);
            if (containsData){
                //recupero de cache los usuarios favoritos si existen y configuro adapter
                setAdapter(getFavoritesUsersList(),recyclerViewFavorite);
            }

        }else {
            aloneFavorite = false;

            String btnTitle = "Añadir " + NUM_ADD_USER + " usuarios más";
            btnAdd = findViewById(R.id.add_users);
            btnAdd.setText(btnTitle);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestData(NUM_ADD_USER, view);
                }
            });
        }
    }

    @Override
    public void getResults(String result) {
        ArrayList<User> usersList = null;
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("results");
            usersList =  new Gson().fromJson(jsonArray.toString(),
                                                new TypeToken<ArrayList<User>>(){}.getType());
        } catch (JSONException e) {
            getErrors(4);//error de conversión
        }

        if (usersList != null){

            if (SessionManager.containsUsersCache()){//añado más usuarios si ya tengo cacheado
                String jsonUsers = SessionManager.getUsersCache();
                ArrayList<User> usersCache =  new Gson().fromJson(jsonUsers,
                        new TypeToken<ArrayList<User>>(){}.getType());
                usersList.addAll(usersCache);

                if (usersList.size()<=LIMIT_USER){//cacheo siempre y cuando no supere el limite
                    saveUsersListCache(usersList);
                }
            }else{//cacheo la primera vez que se ejcuta la app
                saveUsersListCache(usersList);
            }

            setAdapter(usersList, recyclerView);
        }else{
            getErrors(0);
        }

    }

    @Override
    public void getErrors(int error) {
        String messageError = "";
        switch (error){
            case -1: messageError = getResources().getString(R.string.WS_toast_error_API_general);
                break;
            case 0: messageError = getResources().getString(R.string.WS_toast_error_sin_registros);
                break;
            case 1: messageError = getResources().getString(R.string.WS_toast_error_API);
                break;
            case 2: messageError = getResources().getString(R.string.WS_toast_error_usuario);
                break;
            case 3: messageError = getResources().getString(R.string.WS_toast_error_IOexception);
                break;
            case 4: messageError = getResources().getString(R.string.WS_toast_error_JSONexception);
                break;
        }
        Toast.makeText(getApplicationContext(),messageError,Toast.LENGTH_LONG).show();
    }

    @Override
    public void refreshList(ArrayList<User> users, boolean aloneData) {

        if (aloneFavorite){
            setAdapter(getFavoritesUsersList(), recyclerView);
        }else {
            String jsonUsers = new Gson().toJson(users);
            SessionManager.setUsersCache(jsonUsers);
            if (!aloneData) {
                setAdapter(users, recyclerView);
            }
        }
    }

    @Override
    public void refreshListFavorites(ArrayList<User> users) {//solo tablet
        String jsonUsers = new Gson().toJson(users);
        SessionManager.setUsersCache(jsonUsers);
        setAdapter(getFavoritesUsersList(),recyclerViewFavorite);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(isTablet){
            initMenuLandscape(item);
        }else{
            initMenuPortrait(item);
        }

        if (item.getItemId() == R.id.action_clear_cache){
            SessionManager.resetCache();
            setAdapter(new ArrayList<User>(),recyclerView);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initMenuLandscape(MenuItem item) {//version tablet
        if (item.getItemId() == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"No disponible", Toast.LENGTH_LONG).show();
        }
    }

    private void initMenuPortrait(MenuItem item) {//version movil
        if (item.getItemId() == R.id.action_favorite) {

            if (aloneFavorite) {//Vista de todos los usuarios
                ArrayList<User> usersCache = getUsersListCache();
                if (usersCache != null) {
                    setAdapter(usersCache, recyclerView);
                }
                aloneFavorite = false;
                item.setTitle("Solo favoritos");
                btnAdd.setVisibility(View.VISIBLE);

            } else {//Vista de solo los usuarios favoritos
                if (getFavoritesUsersList().size()>0) {
                    setAdapter(getFavoritesUsersList(), recyclerView);//cargamos solo los usuarios favoritos en la lista
                    aloneFavorite = true;
                    item.setTitle("Mostrar todos");
                    btnAdd.setVisibility(View.INVISIBLE);
                }else{
                    Toast.makeText(getApplicationContext(),"No hay usuarios favoritos.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void requestData(int numElem, View view) {
        ArrayList<User> usersCache = getUsersListCache();

        if(usersCache != null) {//solicitud de datos sesion activa
            if (usersCache.size() < LIMIT_USER) {
                new WebServicesAT(this, URL + "/?results=" + numElem).execute();
            } else {
                Snackbar.make(view,"Ya ha alcanzado el límite de usuarios que es " + LIMIT_USER, Snackbar.LENGTH_LONG).show();
            }
        }else{//primera vez que accedemos a la app
            new WebServicesAT(this, URL + "/?results=" + numElem).execute();
        }
    }

    private void setAdapter(ArrayList<User> users, RecyclerView recyclerView) {
        AdapterListUsers adapter = new AdapterListUsers(users, MainActivity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL,false));
    }

    public void saveUsersListCache(ArrayList<User> usersList){
        //ordeno la lista
        Collections.sort(usersList);

        //borro usuarios que tenga el email igual
        for (int i = 0; i < usersList.size()-1; i++) {
            if (usersList.get(i).getEmail().equals(usersList.get(i+1).getEmail())){
                Log.d(TAG,"IGUALES!!!! "+usersList.get(i).getEmail());
                usersList.remove(i);
            }
        }
        //cacheo lista de usuarios en formato json
        String jsonUsers = new Gson().toJson(usersList);
        SessionManager.setUsersCache(jsonUsers);
    }

    public ArrayList<User> getUsersListCache(){
        String jsonUsers = SessionManager.getUsersCache();
        return new Gson().fromJson(jsonUsers,
                new TypeToken<ArrayList<User>>(){}.getType());
    }

    public ArrayList<User> getFavoritesUsersList() {
        ArrayList<User> users = new ArrayList<>();

        ArrayList<User> usersCache = getUsersListCache();
        if (usersCache != null) {
            for (int i = 0; i < usersCache.size(); i++) {
                if (usersCache.get(i).isFavorite()) {//seleccionamos solo users favoritos
                    users.add(usersCache.get(i));
                }
            }
        }
        return users;
    }
}
